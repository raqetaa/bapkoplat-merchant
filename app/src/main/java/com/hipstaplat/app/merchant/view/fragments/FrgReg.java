package com.hipstaplat.app.merchant.view.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.hipstaplat.app.merchant.R;
import com.hipstaplat.app.merchant.view.Prefs;
import com.hipstaplat.app.merchant.view.activities.MainActivity;

/**
 * Created by ershov on 12.10.14.
 */
public class FrgReg extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    private String name;
    private String accountId;

    private LinearLayout login;
    private EditText nameET;
    private EditText accountIdYMET;
    private Button create;

    private Button delete;

    public static FrgReg newInstance(int sectionNumber) {
        FrgReg fragment = new FrgReg();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public FrgReg() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reg, container, false);

        login = (LinearLayout) rootView.findViewById(R.id.login);

        nameET = (EditText) rootView.findViewById(R.id.name);
        accountIdYMET = (EditText) rootView.findViewById(R.id.accountIdYM);
        create = (Button) rootView.findViewById(R.id.create);

        delete = (Button) rootView.findViewById(R.id.delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Prefs.deleteAccount();
                delete.setVisibility(View.GONE);
                login.setVisibility(View.VISIBLE);
            }
        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = nameET.getText().toString();
                accountId = accountIdYMET.getText().toString();
                Prefs.updateAccount(name, accountId);
                delete.setVisibility(View.VISIBLE);
                login.setVisibility(View.GONE);
            }
        });

        if (Prefs.isEmpty()){
            delete.setVisibility(View.GONE);
            login.setVisibility(View.VISIBLE);
        } else {
            delete.setVisibility(View.VISIBLE);
            login.setVisibility(View.GONE);
        }

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }
}
