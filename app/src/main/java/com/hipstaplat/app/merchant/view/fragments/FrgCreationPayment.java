package com.hipstaplat.app.merchant.view.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hipstaplat.app.merchant.R;
import com.hipstaplat.app.merchant.view.Prefs;
import com.hipstaplat.app.merchant.view.activities.MainActivity;
import com.parse.FindCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.ParseException;
import java.util.List;

/**
 * Created by ershov on 12.10.14.
 */
public class FrgCreationPayment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private String comment;
    private String amount;

    private EditText commentET;
    private EditText amountET;
    private Button create;
    private LinearLayout creationLL;
    private TextView ident;

    public static FrgCreationPayment newInstance(int sectionNumber) {
        FrgCreationPayment fragment = new FrgCreationPayment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public FrgCreationPayment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_payment, container, false);
        commentET = (EditText) rootView.findViewById(R.id.commentET);
        amountET = (EditText) rootView.findViewById(R.id.amountET);
        create = (Button) rootView.findViewById(R.id.create);
        ident = (TextView) rootView.findViewById(R.id.ident);

        creationLL = (LinearLayout) rootView.findViewById(R.id.creationLL);

        if (Prefs.isEmpty()){
            commentET.setVisibility(View.GONE);
            amountET.setVisibility(View.GONE);
            create.setVisibility(View.GONE);
        }

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseQuery<ParseObject> query = ParseQuery.getQuery("Payments");
                query.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> parseObjects, com.parse.ParseException e) {
                        if (e == null){
                            String count = parseObjects.size() + "";
                            ParseObject testObject = new ParseObject("Payments");
                            comment = commentET.getText().toString();
                            amount = amountET.getText().toString();
                            testObject.put("paymentId", count);
                            testObject.put("comment", comment);
                            testObject.put("amount", Double.parseDouble(amount));
                            testObject.put("accountId", Prefs.getAccountId());
                            testObject.put("market", Prefs.getName());
                            testObject.put("complited", false);
                            testObject.saveInBackground();
                            creationLL.setVisibility(View.GONE);
                            ident.setVisibility(View.VISIBLE);
                            ident.setText(count);
                        }
                    }
                });


            }
        });
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }
}
