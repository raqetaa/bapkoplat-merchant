package com.hipstaplat.app.merchant;

import android.app.Application;

import com.parse.Parse;

/**
 * Created by ershov on 12.10.14.
 */
public class App extends Application {

    private static App instance;

    public App() {
        instance = this;
        getDBHelper();
    }

    public void onCreate() {
        Parse.initialize(getContext(), "D55dHuZ3ZGAy5oSW3SEGcgWMaYHz7LY6JuBzicCI", "OjLsMc7xmBil0vWTnS0Elfr6h5WWFYWpS9dlZlZd");
    }

    public static App getContext() {
        return instance;
    }

    public static DatabaseHelper getDBHelper(){
        return DatabaseHelper.getInstance(getContext());
    }

}
