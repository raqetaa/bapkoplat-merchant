package com.hipstaplat.app.merchant.view.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.hipstaplat.app.merchant.AdapterHistory;
import com.hipstaplat.app.merchant.R;
import com.hipstaplat.app.merchant.view.Prefs;
import com.hipstaplat.app.merchant.view.activities.MainActivity;
import com.parse.FindCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

/**
 * Created by ershov on 12.10.14.
 */
public class FrgPayments extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    public static FrgPayments newInstance(int sectionNumber) {
        FrgPayments fragment = new FrgPayments();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    private ListView history;

    public FrgPayments() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_payments, container, false);
        history = (ListView) rootView.findViewById(R.id.listHistory);



        ParseQuery<ParseObject> query = ParseQuery.getQuery("Payments");
        query.whereEqualTo("accountId", Prefs.getAccountId());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, com.parse.ParseException e) {
                if (e == null){
                    AdapterHistory adapter = new AdapterHistory(parseObjects);
                    history.setAdapter(adapter);
                }
            }
        });





        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }
}
