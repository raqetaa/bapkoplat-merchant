package com.hipstaplat.app.merchant;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String NAME = "sample.db";

    private static final int VERSION = 1;

    private static DatabaseHelper instance;


    private DatabaseHelper(Context context) {
        super(context, NAME, null, VERSION);
    }

    public static DatabaseHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseHelper(context);
        }
        return instance;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        db.execSQL(createTable());
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // does nothing
    }

    public void saveHistory(
            String merchantCode,
            String comment,
            String amount,
            long time) {
        ContentValues values = new ContentValues();
        values.put("MERCHANT_CODE", merchantCode);
        values.put("COMMENT", comment);
        values.put("AMOUNT", amount);
        values.put("TIME_MILLS", time + "");

        SQLiteDatabase db = this.getWritableDatabase();
        if (db != null) {
            db.insert("HISTORY", null, values);
            db.close();
        }
    }

    public List<History> getAllHistory() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<History> historyArrayList = new ArrayList<>();
        String selectQuery = "SELECT * FROM HISTORY ORDER BY TIME_MILLS DESC";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                History history = new History(
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        Long.parseLong(cursor.getString(4)));
                historyArrayList.add(history);
            } while (cursor.moveToNext());
        }
        return historyArrayList;
    }

    private static String createTable() {
        return "CREATE TABLE IF NOT EXISTS HISTORY (" +
                "id TEXT PRIMARY KEY, " +
                "MERCHANT_CODE TEXT NOT NULL, " +
                "COMMENT TEXT NOT NULL, " +
                "AMOUNT TEXT NOT NULL, " +
                "TIME_MILLS TEXT NOT NULL);";
    }

    public static class History {
        private String merchantCode;
        private String comment;
        private String amount;
        private long time;

        public History(String merchantCode, String comment, String amount, long time) {
            this.merchantCode = merchantCode;
            this.comment = comment;
            this.amount = amount;
            this.time = time;
        }

        public String getMerchantCode() {
            return merchantCode;
        }

        public String getComment() {
            return comment;
        }

        public String getAmount() {
            return amount;
        }

        public long getTime() {
            return time;
        }
    }
}
