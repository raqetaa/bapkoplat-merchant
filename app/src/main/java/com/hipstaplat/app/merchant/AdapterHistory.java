package com.hipstaplat.app.merchant;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.parse.ParseObject;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by ershov on 12.10.14.
 */
public class AdapterHistory extends BaseAdapter {

    public static final SimpleDateFormat DATE = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    private List<ParseObject> list;

    public AdapterHistory(List<ParseObject> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public ParseObject getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {

            convertView = LayoutInflater.from(App.getContext()).inflate(R.layout.item_history_list, null);
            viewHolder = new ViewHolder(convertView);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.build(getItem(position));
        convertView.setTag(viewHolder);

        return convertView;
    }


    private class ViewHolder {
        private TextView code;
        private TextView comment;
        private TextView amount;
        private TextView time;


        ViewHolder(View view) {
            code = (TextView) view.findViewById(R.id.code);
            comment = (TextView) view.findViewById(R.id.comment);
            amount = (TextView) view.findViewById(R.id.amount);
            time = (TextView) view.findViewById(R.id.time);
        }

        public void build(final ParseObject history) {
            code.setText(history.getString("market"));
            comment.setText(history.getString("comment"));
            amount.setText(history.getDouble("amount") + "р");
            time.setText(DATE.format(history.getUpdatedAt()));
        }
    }
}


