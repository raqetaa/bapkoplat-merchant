package com.hipstaplat.app.merchant.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.widget.Toast;

import com.hipstaplat.app.merchant.App;

/**
 * Created by ershov on 12.10.14.
 */
public class Prefs {

    public static void updateAccount(String name, String accountId){
        SharedPreferences userDetails = App.getContext().getSharedPreferences("userdata", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = userDetails.edit();
        edit.clear();
        edit.putString("username", name);
        edit.putString("accountId", accountId);
        edit.commit();
        Toast.makeText(App.getContext(), "...", Toast.LENGTH_LONG).show();
    }

    public static void deleteAccount(){
        SharedPreferences userDetails = App.getContext().getSharedPreferences("userdata", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = userDetails.edit();
        edit.clear();
        edit.putString("username", "");
        edit.putString("accountId", "");
        edit.commit();
        Toast.makeText(App.getContext(), "...", Toast.LENGTH_LONG).show();
    }

    public static boolean isEmpty(){
        SharedPreferences userDetails = App.getContext().getSharedPreferences("userdata", Context.MODE_PRIVATE);
        String accountId = userDetails.getString("accountId", "");
        return TextUtils.isEmpty(accountId);
    }

    public static String getAccountId(){
        SharedPreferences userDetails = App.getContext().getSharedPreferences("userdata", Context.MODE_PRIVATE);
        return userDetails.getString("accountId", "");
    }

    public static String getName(){
        SharedPreferences userDetails = App.getContext().getSharedPreferences("userdata", Context.MODE_PRIVATE);
        return userDetails.getString("username", "");
    }

}
